# Ask Us Anything #1: Wednesday 27, 2022

Dweb Camp [Ask Us Anything Sessions](https://gitlab.com/getdweb/dweb-camp-2022#ask-us-anything) are a space for you to ask us logistical questions, bounce your project ideas off of us (and each other!), and maybe even find other collaborators to work on a project.

## Some Background:
- [Our DWeb Movement](https://getdweb.net/)
- [Our Event](https://dwebcamp.org/)
(August 22-28  in Navarro, CA)
- [Our Event Mission & Pillars](https://dwebcamp.org/our-mission/)
- [Our DWeb Principles](https://getdweb.net/principles)
- [GitLab](https://gitlab.com/getdweb/dweb-camp-2022)
- [Matrix conversation](https://matrix.to/#/!WBhcGXTDMlzyTPWoJv:matrix.org)

## Questions & Answers

- DO I NEED A CAR?
    - No. However, you will need to get to the Bay Area. We will do a rideshare system from the San Francisco/Bay Area, we have a [spreadsheet](https://docs.google.com/spreadsheets/d/1p4wPxLb3tBChn72zQh_EbI5D_6xxEM678Cl59KUnzfo/edit?usp=sharing) to coordinate carpooling. If enough people request it, we will rent a bus and you can buy a round-trip ticket.

- WHAT KIND OF PEOPLE WILL BE ATTENDING?
    - We believe that what unites us is the desire to build a better web. And so there will be a diverse community of people who believe in that: from techies, artists, educators, academics and people from other backgrounds attending DWeb Camp.

- WHAT WAS IT LIKE FOR A TEENAGER & FATHER TO ATTEND IN 2019? (Question presented to one of our prior year attendees)

    - I learned about new projects like Scuttlebutt and Activity Pub. I saw things beyond Bitcoin. I learned alot about off-line tech (like Mesh). He hacked away with Jeromy Johnson, side-by-side with one of the top developers in the Dweb world.

- CAN YOU DESCRIBE THE SPACES AT CAMP NAVARRO?
  - It’s a huge site, about 11 miles from the coast—under giant redwoods. There will be a 4000 square-foot “hackers hall” with electricity & some monitors. There is also a huge shady Redwood Cathedral, where you could hold a workshop, a yoga class, or an art project. There will be various spaces with lots of ways to connect, share, and create.

- WHAT TYPES OF EXPERIENCE DO VOLUNTEERS NEED TO HELP?
    - Volunteers can support with meal service, directing folks to parking spots, supporting the build before or after the Camp which could involve painting signs, setting up spaces, etc. There’s also a need for support which could range from technical assistance to administrative or logistical support. For this event, pricing has taken income into consideration and volunteering can also support that.

- WILL THERE BE A SPACE FOR FELLOWS TO PRESENT THEIR WORK?
    - Yes. Fellows are encouraged to lead workshops and discussions. 

- WHAT IS THE CAMPGROUND TERRAIN LIKE?
  - Some difficulty outside of main camp for anyone using a wheelchair or other accessibility as there are some more rugged hikes, hills, etc. Most things are within 5 minutes of camp and there are three accessible cabins.

- WHAT KIND OF TALKS & SPACES WILL YOU HAVE?
    - This is broad. Some [examples from 2019](https://dwebcamp2019.sched.com/). The goal of camp is to get centered in who we are as well as in the earth.

- IS IT POSSIBLE TO PROPOSE AN ACTIVITY OR SOMETHING ELSE THAT HAPPENS AT THE BUILD?
  - Yes, but remember, there will be only 50 people building and we have a lot of work to do! So if it is a team-building workshop, that would be great, provided the time frame is limited.
