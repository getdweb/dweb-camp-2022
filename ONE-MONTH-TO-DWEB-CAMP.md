# One Month to DWeb Camp: What You Should Be Doing Now

Dear DWeb Campers,

We are so excited to welcome you to DWeb Camp 2022 – a chance to emerge from hibernation after two years and come together around a shared goal:

## *...to connect, learn, share and have fun as we work towards building a better, decentralized web.*

## *A web that actualizes the principles of trust, human agency, mutual respect, and ecological awareness.*

You will soon be receiving an email with packing lists, FAQs, chances to sign up for carpools and sleepovers at the Internet Archive.

Today, I’m writing with a different set of opportunities: *an invitation to share your gifts, ideas, and projects.*

Do you want to give a talk, lead a workshop, or convene a conversation? Lead a yoga class, read a story, perform an informal concert? Here’s how:

- **Give a Lightning Talk:** Fill out [this form](https://docs.google.com/forms/d/e/1FAIpQLSdoSmra1vmsIGkrpIVaS2JxRfpaO-vS-1NkFUNeQfTSMao-pw/viewform)

- **Propose a workshop, activity, or conversation:** Fill out [this form](https://docs.google.com/forms/d/e/1FAIpQLSfzhhPhcoiVQ_-Iltmwx34XihhfuugCeaNb-ILMWcTLw0wahg/viewform)

- **Share your ideas via our GitLab:** Fill out this [New Issue](https://gitlab.com/getdweb/dweb-camp-2022/-/issues/new?issuable_template=propose-a-project-template) template in our [GitLab Issues page](https://gitlab.com/getdweb/dweb-camp-2022/-/issues), and see everyone else’s ideas too.
  
Some organizers are working on **big collaborative ideas**. Here are a few you can sign up for:

- **Science Fair by the Necto Labs & [Mysterium Network](https://www.mysterium.network/):** Present your tool, project, or campaign by [signing up here](https://forms.gle/tYdoCqS3RLKrnZFR8) for a camp-wide expo
 - **Dogfooding Decentralization by [dSocial Commons](https://twitter.com/dsocialcommons):** Need feedback on your tech? [Sign up here](https://forms.gle/tYdoCqS3RLKrnZFR8) to participate in a camp-wide user testing session
- **[P-2-P Space](https://github.com/fluencelabs/DWeb-camp-peer-to-peer/blob/main/README.md) by Eric Bear & Anna Lekanova**: Got a interactive social or digital technology that fosters person-to-person relational networks to do a session with? [Tell us about it here!](https://docs.google.com/forms/d/e/1FAIpQLSew5pMVZq6W8-VcIBQt7o0HwOUF8cJDMdlw9iRha7t8J2Y5hg/viewform)

**Other camp highlights:**
- [DWEEBS -- a Reverse Conference @ DWeb](https://mixmix.gitlab.io/dweebs/#/README), where you watch talks now, so you can connect to people 100% of the time at Camp, by **[Mix Irving](https://twitter.com/whimful)**
- Communal singing rituals led by artist and opera singer **[Amelia Winger-Bearskin](https://twitter.com/ameliawb)**
- [Introduction to taiji](https://gitlab.com/getdweb/dweb-camp-2022/-/issues/11), the ancient practice of movement by **Charlton Lee**, a 20th generation disciple of Chen-style taiji
- Talk by **[Nathan Schneider](https://twitter.com/ntnsndr)** on “How Community Governance should feel”
- Morning coffee & conversation over “[insightful beans](https://gitlab.com/getdweb/dweb-camp-2022/-/issues/7)” about preserving privacy & ethical data collection
- [Cryptographically Verified Scavenger Hunt]() by the folks at **[The Guardian Project](https://guardianproject.info/)**
- Black Box Governance Live Action Role Play (LARP) by Berlin-based collective **[Black Swan](https://trust.support/feed/black-swan)**
- [Data Interoperability with Solid & Beyond](https://gitlab.com/getdweb/dweb-camp-2022/-/issues/9), a workshop by **Jackson Morgan**.
- Conversation about [Decentralized action for global accountability](https://gitlab.com/getdweb/dweb-camp-2022/-/issues/8) by **John Ryan** & **Mai Ishikawa Sutton**
- [Onboard to Secure Scuttlebutt](https://gitlab.com/getdweb/dweb-camp-2022/-/issues/6), the offline-first, local-first, decentralised, resilient platform to connect with your friends; session led by **[Planetary](https://www.planetary.social/)**, **[Manyverse](https://www.manyver.se/)** & assorted **Scuttlebutts**
- And if you missed the **July Meet-up**, [here's a video](https://archive.org/details/meetup-july-19th-1) of the 6 projects who shared their plans for our gathering in the redwoods.

We can’t wait to see what you want to share!

Contact [dwebcamp@archive.org](dwebcamp@archive.org) with any questions.
