# FAMILIES: Welcome to DWeb Camp! Please tell us about your children!

Dear Campers with Kids,
We are so excited to welcome you to DWeb Camp 2022! We have always envisioned this experience to be centered around the future: our children.

  ![](images/children.png)
In 2018, we first met Galileo at our Decentralized Web Summit. And we are so excited that he and 15 other children (under the age of 12) are joining us this year.
Not to mention six teenagers!

I want to introduce you to the Stewards of our Children's programming, **Andi Wong** and **Patrick Wu**.

A bit about Andi:

###  Andi Wong is excited to steward the “BE WATER” Way Station at DWeb Camp, a welcoming place for learners of all ages to create, collaborate, and go with the flow. Andi served as teaching artist and site arts coordinator in San Francisco public schools for over two decades. As project coordinator for ArtsEd4All, she creates curriculum, conducts workshops, hosts film screenings, and organizes participatory community events such as the annual Blake Mini Library book drive for Hamilton Families, Civic Season with Made By Us, and open-ended play with The Blue Marbles Project. Her creative partners include composer/musician Marcus Shelby, First Voice led by artistic directors Brenda Wong Aoki and Mark Izu, The Last Hoisan Poets (poets Genny Lim, Flo Oy Wong and Nellie Wong), Del Sol String Quartet, and the Internet Archive.

Patrick is a college student and personal trainer who loves working with kids, and has the energy to keep up with them.

Andi has so much planned for your wonderful children. **But first we'd like to know more about them. Can you share a bit: their names, ages, passions, special interests? Anything special we should know (for instance, a birthday or two?)**.

Then, I'll let Andi and Pat tell you more about what they have planned. 

**One note**: in general, we'll want you to accompany your young children to all activities, except from 7-8:30 PM when we'll host family activities inside the library. During this period, you can drop off the kids for movies, games, crafts, storytime, shadow puppetry and step out for Cocktail Hour or just some chill & relax time. Tweens and teens should be fine at Archery, Rock Wall and (maybe even) Axe Throwing Class by themselves.

Some key links:

    [FAQ](https://dwebcamp.org/faq/#families_and_pets) about kids
    [Search in Sched](https://dwebcamp2022.sched.com/?searchstring=great+for+kids&iframe=no&w=&sidebar=&bg=) for "Great for Kids" to see activities specially suited for your children.

Thanks for bringing the family to DWeb Camp! Send us any questions!

best,
Wendy
