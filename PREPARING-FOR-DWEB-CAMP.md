# Preparing for DWeb Camp: Packing, Arrival Times, Ride Shares & More

Dear DWeb Camper, 

We are so excited to welcome you to the redwoods this August! Here is some important information you’ll need to prepare.

## Arrival & Departure
Gates open for registration on <u>Wednesday, August 24th at 2:00pm</u>, and close at for the day at 10:00pm. If you cannot make it before 10:00pm please come up on Thursday, August 25th when gates open again at 9:00am.

Orientation & Opening Circle is <u>Wednesday, August 24th at 7:30pm</u>. The Opening Circle is the best way to get centered for camp. We hope you can prioritize being at camp for this beautiful ritual.

Camp is over on <u>Sunday, August 28th at 11:30am</u>, and all campers need to be off site by this time. The only exception to this is if you would like to actively help with breakdown and join a breakdown crew. Signups will be available at the event. 

We suggest ride sharing and you can [sign up to offer a ride, or request one](https://docs.google.com/spreadsheets/d/1p4wPxLb3tBChn72zQh_EbI5D_6xxEM678Cl59KUnzfo/edit#gid=0). If you bring three people in your car, parking is free.

If you want to sleepover (literally in a sleeping bag on the floor) the night before or after the camp, we’re opening the doors to the Internet Archive. Please [sign up here](https://fs26.formsite.com/ArchiveHR/IA_sleepover_2022/index.html).

## [Packing List](https://dwebcamp.org/faq/#getting_ready_for_camping-2)
- Active, weather-appropriate, layered apparel:
    - Shorts
    - Bathing suit
    - Shirts
    - Light jacket
    - Long pants/jeans
    - Extra change of clothes
    - Tennis Shoes or Hiking Shoes
    - Comfortable shoes or sandals to wear around camp.
    - Flashlight or headlamp
    - Re-usable water bottle
    - Sunglasses
    - Hat
- Miscellaneous Items:
    - Musical instrument
    - Any needed medical supplies / prescriptions. There will be power for CPAP machines.
    - Toiletries // Shampoo, toothbrush & paste, sunscreen (biodegradable)
    - Towel
    - Day pack or fanny pack to carry your stuff around
    - A book, or reading material to enjoy
    - In Deluxe/Regular tents, linens are provided, but if you have a favorite pillow or blanket, feel free to bring them.
    - Beverages and snacks of your choice
    - Vial of water from your home locale (more on that later!)

## Meals
We’ll be offering you three hearty meals each day with vegetarian, vegan and non-dairy options. But we encourage you to bring extra snacks and beverages to share.

We’ll be setting up a DIY coffee/tea/cocktail station to get everyone going. But please bring your favorite teas and alcohol of choice!

At each meal there will be kid-friendly bread, peanut butter, and other favorites. But if your children have particular palettes, please bring foods you know they will enjoy.

## Covid Policy
Wow, it’s a shifting landscape when it comes to COVID. But we are hopeful that the current wave of the B.5 variant will be waning by late August. [Here is our current policy](https://dwebcamp.org/covid/). 

Everyone is required to take a COVID test within 24-hours of arriving at Camp. It’s a long drive, so we suggest you take the test before setting out. <u>To maintain COVID safety, we will not alow drop-in visitors</u>. We need your help to keep everyone safe.

## Getting Involved
The magic of DWeb is in our community and what everyone brings to camp. We are accepting submissions for presentations, performances, workshops and lightning talks. If you would like to contribute in any way, here is [how to get started](https://gitlab.com/getdweb/dweb-camp-2022/-/blob/main/ONE-MONTH-TO-DWEB-CAMP.md).

## Water Policy
We know how important water is. Water is life. When you arrive at Camp Navarro, you’ll realize the area is in a historic drought: the Navarro River is a trickle. In Mendocino County, preserving water is so important, we will <u>not</u> be washing dishes. Instead we will use compostable tableware.

DWeb Camp is also a chance to connect to the land itself. One important theme of our programming is *Healing Water*, led by Native artist and technologist, Amelia Winger-Bearskin. We ask that each participant bring a small (under 3 oz) vial of water from home. It can be sacred water from a favorite source or tap water from your kitchen sink. We’ll be testing the quality of this water, mapping it, and using it in our opening and closing ceremonies.

Over the months, people have asked us every sort of question. [Here is an FAQ](https://dwebcamp.org/faq/) compiling them all. Or attend the [August 4 DWeb Meetup](https://www.eventbrite.com/e/dweb-august-meetup-projects-and-presentations-at-dweb-camp-tickets-388804303137) and ask us your question in person.  Write us at dwebcamp@archive.org.

Can’t wait to connect with you under the giant redwoods!

Warmly,


Wendy Hanamura

DWeb Camp Organizer
